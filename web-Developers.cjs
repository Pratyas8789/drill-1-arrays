let data = require('./1-arrays-jobs.cjs')

const webDevelopers = data.filter((userData) => userData['job'].includes("Web Developer")).map(((userData) => userData['first_name'] + " " + userData['last_name']))

console.log(webDevelopers);
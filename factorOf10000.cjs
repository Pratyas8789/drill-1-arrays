let data = require('./1-arrays-jobs.cjs')

const correctedSalary = data.map((userData) => {
    userData["corrected_salary"] = +userData["salary"].replaceAll("$", "") * 10000;
    return (userData);
});

console.log(correctedSalary);
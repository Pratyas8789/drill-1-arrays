let data = require('./1-arrays-jobs.cjs')

const sumOfAllSarlyWithCountry = data.reduce((acc, data) => {
    const sarly = +data.salary.replaceAll('$', '');
    if (acc[data.location]) {
        acc[data.location] += sarly;
    } else {
        acc[data.location] = sarly;
    }
    return acc;
}, {});

console.log(sumOfAllSarlyWithCountry);
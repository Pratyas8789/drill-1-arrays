let data = require('./1-arrays-jobs.cjs');

const sarlyWithCountry = data.reduce((acc, data) => {
    let sarly = (+(data.salary.replaceAll('$', '')));
    if (acc[data.location]) {
        acc[data.location][0] += sarly;
        acc[data.location][1]++;
    } else {
        const arr = [];
        arr[0] = sarly;
        arr[1] = 1;
        acc[data.location] = arr;
    }
    return acc;
}, {});

for(let key in sarlyWithCountry){
    sarlyWithCountry[key]=+(sarlyWithCountry[key][0]/sarlyWithCountry[key][1]).toFixed(2);
}
console.log(sarlyWithCountry);
let data = require('./1-arrays-jobs.cjs')

const SumOfSalary = data.reduce((acc, currData) => acc += +currData["salary"].replaceAll("$", ""), 0);

console.log(SumOfSalary.toFixed(2));